// Assignment 2
// Nick Singstock

TASK 1:
	
	1. 	Parallelization has been added to the minimum and maximum finding 'for' 
		loops which scan through the x vector. Each thread investigates a unique index
		from the vector, which is scanned through linearly, and compares it against the 
		current max and min values which are defined as critical values so that if one
		thread wants to rewrite the value at that memory location, the other threads cannot
		attempt to do the same thing during the same time, avoiding the race condition, 
		this is done by using reductions in the for loop.
	
		During the actaul histogram bin allocating, another parallel omp for loop is used
		where the x vector is again scanned through linearly, and each thread investigates
		a specific index. A given thread then decides which bin to place the given index into, 
		and to avoid race condition, assignment to the bin_count variable is maintained as a 
		critical operation.
		
	2.	The only sequential section in my code are getting the overall length of the x vector,
		allocating memory space to the min and max variables and defining the bin boundaries.
		These are all likely to be very fast sections as they only scale with the number of bins, 
		which is typically small. None of these functions will be slowed down by a larger x vector,
		resulting in a small Ts, or sequential section. During the parallel section, there will
		not be perfect scaling however as assignment of critical variables requires coordination 
		between the threads. It is still likely that the overall Speedup, S, will likely be large 
		since there is strong scaling in this problem, however due to Amdahl's law, adding more 
		processors, p, will eventually reach a bottleneck and Tp will not be able to be improved
		beyone a certain limit.
		
		
TASK 3:
1.a)		
		
		k=0		k=1				k=2					k=3				k=4
		x^1 -> x^2 = x^1*x^1 -> x^4 = x^2*x^2 -> x^8 = x^4*x^4 -> x^16 = x^8*x^8 -> etc.
		Assuming memory reading and writing is free, every step must occur sequentially
		and duplication of a previous variable does not require work, thus one work process
		(multiplication) occurs at every step and work, W, is equal to k. Depth is therefore 
		also equal to k due to the sequential nature of the algorithm which is based on every 
		additional steps reliance on the output of the previous step. No parallelization can 
		occur in this system. 
		(I'm not sure what you meant by draw but hopefully this is close, I can't actually 
		draw in Notepad)
		
1.b)	

		x = M x M matrix. 
		ex.     x = [ 1 3 ]		x^2 = 	[ 1*1+3*3  1*3+3*4 ]	= 	[ 10 15 ]
		M = 2		[ 2 4 ]				[ 2*1+4*2  2*3+4*4 ] 		[ 10 22 ]
		
		Since every entry of the matrix requires M multiplications and M-1 additions at every, k,
		step there is far more work occuring in this process. There are M^2 entries so the work 
		at each step is M*M*(2*M-1) for a total work, W = k*M*M*(2*M-1). Assuming we have many 
		processors to work with, the depth does not have to be this large as each matrix entry can 
		be calculated individually without communication until the proceeding step. Additionally,
		each multiplication is independent within each calculation, so these can be further divided
		into subprocesses; the addition of these values can also be divided once they are tabulated.
		Assuming unlimited processors, depth = k * ( 1 + math.ceil( log_2( M ) ) ). The k is
		required since each step, k, depends on the previous, fully defined matrix x. The 1 is to
		include all of the multiplication occuring for every matrix entry, all at once, and the 
		base 2 log is to handle all of the addition of the multiplication results to determine
		the new matrix entries, which are free to assign to memory.

		
2 		
		
		For a parallel sum PRAM algortihm, work = Tserial = n, depth/time = Tpar. = log_2(n)
		so speedup = Ts / Tp = n / log_2(n) and efficiency = n / p*log_2(n).
		
3		

		Efficiency, E = Ts / (p * Tp) = n / p*(n/p + log(p)) = n / (n + p*log(p))
		increase p to k*p and maintain E, what is new n?
		
		n / (n + kp*log(kp)) = n / (n + p*log(p))
		
		x = scaling factor for n = k * log(kp)/log(p)
		
		as k -> infinity (unlimited processors), x -> infinity, meaning n would need to be infinitely
		fast in order to maintain the same efficiency, so the program is not scalable indefinitely.
		
4		

		The pattern developed by the presented 'for' loop is the triangular number sequence so this
		for loop can be exploited and parallelized by using the equation for this number sequence.
		Using the equation: a[i] = i*(i+1)/2 removes dependence upon the previous loop itteration,
		making the loop fully parallelizable.