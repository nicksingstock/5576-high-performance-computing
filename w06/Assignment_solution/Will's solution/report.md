# CSCI 4576 (High-Performance Scientific Computing)
## Homework #6
## Author: Will Shand
### Task 1
#### Question 1.1
Investigate the code for both histogram and quicksort and write a short summary for each. Describe the difference between your implementation and these.

##### Answer 1.1
The provided quicksort and histogram implementations are quite different from mine, since I decided to avoid the suggested implementations of these functions and instead made implementations that traded some speed for a much smaller memory footprint.

First, my quicksort implementation did a parallel pivot on subarrays of size `O(n/p)`, and then swapped subarrays that were below the pivot element from the end of the array with subarrays above the pivot at the array's beginning. This quicksort implementation does a parallel scan over the elements of the array being sorted. It creates an array `x` for which the `i`th element is equal to the number of elements of the array in the index range `1`, `2`, ..., `i` that are below the pivot element. The results of the scan are then used to figure out where to swap elements into the pivoted array. The result is that this code is a bit faster than mine, but it requires much more memory to do the scans.

In my histogram code, I sorted subarrays of `x` with size `O(n/p)`, and then looped over the `bin_bdry` vector. I did a binary search on the subarrays of `x` to find how many elements of `x` belonged to each bin, and then totalled the reseults of those searches to build the histogram. The histogram implementation provided here builds an array of unique bins for every OpenMP thread. It then loops over the elements of `x` and does a binary search on the `bin_bdry` array to find the appropriate histogram bucket for every element. The OpenMP thread handling this element then updates the number of items in its local bucket vector. Once all of the elements of `x` have been counted, we sum over the local buckets of all of the threads to find the total number of elements in each histogram bin. This has better computational complexity than my histogram implementation, and doesn't destroy any ordering of the vector `x`. On the other hand, it requires `O(pm)` memory (where `m` is the number of histogram bins), whereas my version uses essentially only a small constant amount of additional memory for every additional thread.

#### Question 1.2
Repeat the scaling runs on Summit for both programs

For histogram, perform strong and weak scaling analysis on a single node on Summit (set `OMP_NUM_THREADS` to 1, 2, 4, 8, 12, 16, 20, 24) for `n=1E2,1E5,1E9` and number of bins `num_bins=10,1E3,1E4` with two data sets:

1. Uniformly distributed doubles between `[0,1]`
2. Uniformly distributed doubles between `[0,1]` and an outlier with value equal to `num_bins` (the third command line argument to `test_histogram.exe` is the outlier).

For quicksort, perform strong and weak scaling analysis on a single node with the number of threads listed above. For weak scaling use grain sizes of 1M and 10M long integer per thread. For strong scaling, work with a problem of size `(number of physical cores) x {1M,10M}`. Report your scaling analysis as plots of wall-clock-time versus number of cores.

Make sure you set `cpus-per-task` to `24` in your job file. Shell `for` loop is a quick way to get most of the runs you need, e.g.,

```bash
# Strong scaling
$ for i in {1,2,4};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe 1000000;done
# Weak scaling
$ for i in {1,2,4};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe $((1000000*$i));done
```

##### Answer 1.2
**__Histogram scaling analysis__**: I tried running the histogram node for different numbers of threads, numbers of histogram bins, and array sizes. Using the collected data, I performed weak and strong scaling analyses for the provided histogram implementation.

In general there was fairly little correlation between the number of bins `n` and the runtime (since the computational complexity of this algorithm only increases as log the number of bins), so to do the weak scaling analysis I only fixed the ratio of the number of number of array elements `m` relative to the number of threads. I did a weak scaling analysis for three different numbers of bins (10, 1000, and 10000), both with data randomly distributed in the interval `[0,1]` and randomly distributed data with an outlier equal to the number of bins. (*Note*: I only went up to `m = 1e8*p` instead of `m = 1e9*p` since at that point the system seemed to start having issues with memory starvation).

*Time (secs) required to compute histogram for `n = 1e1` bins and fixed ratio of array elements per number of threads*:

|         | `m = 1e2*p` | `m = 1e5*p` | `m = 1e8*p` | `m = 1e2*p` (+outlier) | `m = 1e5*p` (+outlier) | `m = 1e8*p` (+outlier) |
|---------|-------------|-------------|-------------|------------------------|------------------------|------------------------|
| `p = 1` |    `4.46e-5`|    `1.37e-3`|    `1.30e+0`|               `1.95e-5`|               `1.37e-3`|               `1.30e+0`|
| `p = 2` |    `1.39e-4`|    `1.50e-3`|    `2.39e+0`|               `1.25e-4`|               `1.58e-3`|               `1.32e+0`|
| `p = 4` |    `2.16e-4`|    `1.79e-3`|    `1.33e+0`|               `1.82e-4`|               `1.65e-3`|               `1.35e+0`|
| `p = 8` |    `4.48e-4`|    `1.95e-3`|    `1.46e+0`|               `3.98e-4`|               `1.83e-3`|               `1.68e+0`|
| `p = 12`|    `5.14e-4`|    `2.19e-3`|    `1.55e+0`|               `5.81e-4`|               `2.27e-3`|               `1.67e+0`|
| `p = 16`|    `6.67e-4`|    `2.17e-3`|    `1.72e+0`|               `1.01e-3`|               `2.61e-3`|               `1.74e+0`|
| `p = 20`|    `1.31e-3`|    `2.52e-3`|    `1.76e+0`|               `9.07e-4`|               `2.68e-3`|               `1.76e+0`|
| `p = 24`|    `5.48e-3`|    `1.60e-2`|    `1.87e+0`|               `2.36e-3`|               `1.44e-2`|               `1.89e+0`|


*Time (secs) required to compute histogram for `n = 1e3` bins and fixed ratio of array elements per number of threads:*

|         | `m = 1e2*p` | `m = 1e5*p` | `m = 1e8*p` | `m = 1e2*p` (+outlier) | `m = 1e5*p` (+outlier) | `m = 1e8*p` (+outlier) |
|---------|-------------|-------------|-------------|------------------------|------------------------|------------------------|
| `p = 1` |    `3.17e-5`|    `1.42e-3`|    `1.30e+0`|               `3.10e-5`|               `1.38e-3`|               `1.30e+0`|
| `p = 2` |    `1.02e-4`|    `1.58e-3`|    `1.40e+0`|               `9.95e-5`|               `1.69e-3`|               `1.37e+0`|
| `p = 4` |    `1.31e-4`|    `1.48e-3`|    `1.41e+0`|               `1.22e-4`|               `1.60e-3`|               `1.39e+0`|
| `p = 8` |    `1.75e-4`|    `1.78e-3`|    `1.52e+0`|               `1.27e-4`|               `1.80e-3`|               `1.57e+0`|
| `p = 12`|    `1.69e-4`|    `1.95e-3`|    `1.69e+0`|               `1.84e-4`|               `1.83e-3`|               `1.67e+0`|
| `p = 16`|    `2.02e-4`|    `1.93e-3`|    `1.81e+0`|               `1.37e-4`|               `1.90e-3`|               `1.78e+0`|
| `p = 20`|    `2.20e-4`|    `2.10e-3`|    `1.82e+0`|               `1.98e-4`|               `3.56e-3`|               `3.28e+0`|
| `p = 24`|    `2.98e-4`|    `2.35e-3`|    `1.90e+0`|               `2.57e-4`|               `2.09e-3`|               `1.90e+0`|

*Time (secs) required to compute histogram for `n = 1e4` bins and fixed ratio of array elements per number of threads*:

|         | `m = 1e2*p` | `m = 1e5*p` | `m = 1e8*p` | `m = 1e2*p` (+outlier) | `m = 1e5*p` (+outlier) | `m = 1e8*p` (+outlier) |
|---------|-------------|-------------|-------------|------------------------|------------------------|------------------------|
| `p = 1` |    `1.91e-4`|    `1.63e-3`|    `1.59e+0`|               `2.63e-4`|               `1.36e-3`|               `1.30e+0`|
| `p = 2` |    `2.83e-4`|    `1.68e-3`|    `1.82e+0`|               `4.84e-4`|               `1.50e-3`|               `1.31e+0`|
| `p = 4` |    `1.86e-4`|    `1.74e-3`|    `1.75e+0`|               `2.38e-4`|               `1.49e-3`|               `1.49e+0`|
| `p = 8` |    `4.89e-4`|    `1.90e-3`|    `1.77e+0`|               `2.73e-4`|               `1.74e-3`|               `1.53e+0`|
| `p = 12`|    `3.38e-4`|    `2.04e-3`|    `1.87e+0`|               `3.35e-4`|               `1.88e-3`|               `1.56e+0`|
| `p = 16`|    `6.98e-4`|    `2.19e-3`|    `1.97e+0`|               `6.06e-4`|               `1.76e-3`|               `1.80e+0`|
| `p = 20`|    `4.07e-4`|    `2.30e-3`|    `2.21e+0`|               `4.07e-4`|               `2.30e-3`|               `2.21e+0`|
| `p = 24`|    `4.52e-4`|    `2.39e-3`|    `2.19e+0`|               `6.76e-4`|               `1.98e-3`|               `1.89e+0`|

From these data it appears that the histogram implementation has very good weak scaling. The runtime as we increased the number of threads didn't change too much between one and twenty-four threads -- e.g. for `n = 1e4` bins and `m = 1e8` array elements/thread, the runtime with 24 threads was only 45% higher (which is relatively low compared to e.g. quicksort) than it was with one thread. This suggests that there's relatively low parallelization overhead in this code, and that efficiency deprecation from increasing the number of threads are quite small.

To do the strong scaling analysis, I fixed the number of elements (i.e. the number of array elements to compute the histogram for was constant in the number of threads) and increased the number of threads from 1 to 24. The resulting data for `n=1e1` bins, `n=1e3` bins, and `n=1e4` bins is shown in the tables below:

*Time (secs) required to compute histogram for `n = 1e1` bins and fixed number of array elements*:

|         | `m = 1e2` | `m = 1e5` | `m = 1e9` | `m = 1e2` (+outlier) | `m = 1e5` (+outlier) | `m = 1e9` (+outlier) |
|---------|-----------|-----------|-----------|----------------------|----------------------|----------------------|
| `p = 1` |  `4.28e-5`|  `1.46e-3`|  `1.37e+1`|             `1.89e-5`|             `1.34e-3`|             `1.30e+1`|
| `p = 2` |  `1.71e-4`|  `8.04e-4`|  `6.69e+0`|             `1.98e-4`|             `1.01e-3`|             `6.63e+0`|
| `p = 4` |  `2.21e-4`|  `7.01e-4`|  `3.33e+0`|             `2.78e-4`|             `6.08e-4`|             `3.34e+0`|
| `p = 8` |  `4.04e-4`|  `6.53e-4`|  `1.82e+0`|             `4.47e-4`|             `6.84e-4`|             `1.82e+0`|
| `p = 12`|  `7.33e-4`|  `6.52e-4`|  `1.25e+0`|             `6.28e-4`|             `7.86e-4`|             `1.29e+0`|
| `p = 16`|  `6.96e-4`|  `9.23e-4`|  `1.03e+0`|             `6.50e-4`|             `8.33e-4`|             `9.81e-1`|
| `p = 20`|  `8.38e-4`|  `8.69e-4`|  `8.92e-1`|             `7.50e-4`|             `9.70e-4`|             `9.04e-1`|
| `p = 24`|  `7.42e-3`|  `1.03e-3`|  `8.19e-1`|             `1.04e-3`|             `2.94e-3`|             `8.13e-1`|

*Time (secs) required to compute histogram for `n = 1e3` bins and fixed number of array elements*:

|         | `m = 1e2` | `m = 1e5` | `m = 1e9` | `m = 1e2` (+outlier) | `m = 1e5` (+outlier) | `m = 1e9` (+outlier) |
|---------|-----------|-----------|-----------|----------------------|----------------------|----------------------|
| `p = 1` |  `2.87e-5`|  `1.43e-3`|  `1.30e+1`|             `2.91e-5`|             `1.50e-3`|             `1.31e+1`|
| `p = 2` |  `2.96e-4`|  `1.06e-3`|  `6.80e+0`|             `2.55e-4`|             `9.55e-4`|             `6.75e+0`|
| `p = 4` |  `2.93e-4`|  `6.13e-4`|  `3.37e+0`|             `2.82e-4`|             `7.60e-4`|             `3.36e+0`|
| `p = 8` |  `5.99e-4`|  `5.96e-4`|  `1.87e+0`|             `3.89e-4`|             `5.92e-4`|             `1.83e+0`|
| `p = 12`|  `6.10e-4`|  `7.72e-4`|  `1.26e+0`|             `5.56e-4`|             `6.81e-4`|             `1.46e+0`|
| `p = 16`|  `8.12e-4`|  `8.44e-4`|  `1.16e+0`|             `7.24e-4`|             `9.30e-4`|             `1.13e+0`|
| `p = 20`|  `8.49e-4`|  `1.03e-3`|  `8.61e-1`|             `1.13e-3`|             `1.43e-3`|             `9.28e-1`|
| `p = 24`|  `2.45e-3`|  `1.11e-3`|  `7.68e-1`|             `3.19e-3`|             `1.18e-2`|             `7.91e-1`|

*Time (secs) required to compute histogram for `n = 1e4` bins and fixed number of array elements*:

|         | `m = 1e2` | `m = 1e5` | `m = 1e9` | `m = 1e2` (+outlier) | `m = 1e5` (+outlier) | `m = 1e9` (+outlier) |
|---------|-----------|-----------|-----------|----------------------|----------------------|----------------------|
| `p = 1` |  `2.61e-4`|  `1.80e-3`|  `1.52e+1`|             `3.28e-4`|             `1.63e-3`|             `1.37e+1`|
| `p = 2` |  `5.30e-4`|  `1.17e-3`|  `7.97e+0`|             `3.00e-4`|             `1.14e-3`|             `7.16e+0`|
| `p = 4` |  `4.82e-4`|  `9.35e-4`|  `3.95e+0`|             `4.35e-4`|             `7.40e-4`|             `3.52e+0`|
| `p = 8` |  `5.86e-4`|  `8.82e-4`|  `2.29e+0`|             `5.29e-4`|             `8.60e-4`|             `2.07e+0`|
| `p = 12`|  `9.25e-4`|  `9.88e-4`|  `1.61e+0`|             `7.93e-4`|             `9.15e-4`|             `1.32e+0`|
| `p = 16`|  `1.07e-3`|  `1.16e-3`|  `1.39e+0`|             `1.06e-3`|             `1.06e-3`|             `1.18e+0`|
| `p = 20`|  `1.17e-3`|  `1.29e-3`|  `1.02e+0`|             `1.33e-3`|             `1.38e-3`|             `9.48e-1`|
| `p = 24`|  `3.51e-3`|  `3.98e-3`|  `9.47e-1`|             `1.42e-3`|             `1.23e-2`|             `8.31e-1`|

As I found when I did this assignment, the presence (or lack thereof) of an outlier has virtually no effect on running `histogram`, and the increase in runtime with the number of bins is small -- almost to the point of being imperceptible -- for small `n`. For large `n`, adding more bins can increase runtime quite a bit.

I found that overall this implementation has good strong scaling. To test this, I analyzed the non-outlier data for the runtime versus the number of threads for `n = 1e4` bins and for both `m = 1e5` array elements and `m = 1e9` array elements. For `m = 1e5`, it appears that runtime decreases up to eight threads, and then starts increasing again. However, this is likely due to the fact that the code already runs extremely fast (less than .001 seconds) at this point, and so even low overhead from having additional threads boosts runtime. With `m = 1e9` array elements, the amount of time required for `histogram` to run is much longer, and so the runtime consistently decreases with any increase in the number of threads. To analyze this, I regressed an hyperboal against the number of threads versus runtime data (shown in the graphs below); specifically, I did a linear regresion on number of threads versus `1 / runtime` and then transformed the results back to our original variables. The fit I got for this model is roughly `1 / runtime = .04357 * threads + .05844`. One way of looking at this is to say that adding one more thread decreases runtime by about 29.9% when we have 1 thread, 7.0% when we have 12 threads, and 3.8% when we have 24 threads; another is that doubling the number of threads starts by giving a roughly 29.9% performance boost but quickly approaches a 50% boost asymptotically (43.8% with 4 threads, 47.0% with 12 threads, and 48.6% with 24 threads), at least according to the model.

Across the different numbers of threads, these results seem to suggest that we get very good strong scaling. If the scaling was poorer, we would find that it takes a while for us to reach that aforementioned 50% threshold (or, more likely, that we reach optimal runtime quickly and that overhead causes runtime to start increasing again). In the linear regression, poor scaling manifests as a low intercept-to-slope ratio ("low" being interpreted as meaning quite a bit more than one); here that was not a problem, as this ratio was about 1.34.

![Histogram strong scaling graph](images/histogram_strong_scaling.png)

**__Quicksort scaling analysis__**: I performed both weak and strong scaling analyses for the quicksort implementation that was provided. For the weak scaling analysis, I kept the number of array elements per thread fixed as I increased the number of threads from 1 to 24:

|          | `n = 1M * p` | `n = 10M * p` |
|----------|--------------|---------------|
| `p = 1`  |    `1.41e+0` |     `1.44e+1` |
| `p = 2`  |    `1.88e+0` |     `1.94e+1` |
| `p = 4`  |    `2.41e+0` |     `2.42e+1` |
| `p = 8`  |    `2.97e+0` |     `3.14e+1` |
| `p = 12` |    `3.81e+0` |     `3.88e+1` |
| `p = 16` |    `4.99e+0` |     `5.08e+1` |
| `p = 20` |    `6.42e+0` |     `6.33e+1` |
| `p = 24` |    `1.18e+1` |     `1.22e+2` |

If we only take these data at face value, it doesn't appear that the weak scaling of this algorithm is that great. To test this, I fit an exponential curve to the data (by taking a logarithmic transform of the runtimes and performing a linear regression on the number of threads versus the log-transformed times) for both 1M array elements/thread and 10M elements/thread. The runtime was highly correlated with the number of threads (`r > 0.98` for both regressions) and in both cases I found an approximately 8.2% increase in runtime for adding one more thread. Since the average-case sequential runtime for quicksort is superlinear -- `O(n log n)` -- this isn't entirely unreasonable. It's fairly comparable to the increases I got when I implemented quicksort -- around 6% per additional thread.

![Weak scaling graphs](images/quicksort_weak_scaling.png)

In order to perform the strong scaling analysis, I fixed the number of array elements and scaled the number of threads. The results are shown in the table below:

|          |     `n = 1M` |     `n = 10M` |
|----------|--------------|---------------|
| `p = 1`  |    `1.41e+0` |     `1.44e+1` |
| `p = 2`  |    `9.50e-1` |     `9.84e+0` |
| `p = 4`  |    `5.85e-1` |     `6.08e+0` |
| `p = 8`  |    `3.90e-1` |     `3.87e+0` |
| `p = 12` |    `2.80e-1` |     `3.04e+0` |
| `p = 16` |    `3.14e-1` |     `3.12e+0` |
| `p = 20` |    `3.10e-1` |     `3.15e+0` |
| `p = 24` |    `4.64e-1` |     `4.83e+0` |

At first the scaling seems to be pretty good: as we increase the number of threads from 1 to 12, adding one more thread reduces the runtime by about 13% on average (with even larger gains for small numbers of threads). However, these gains are lost when we go past twelve threads. At that point, it appears that thread creation and communication exceeds the performance gain of having additional threads. This is a little surprising, especially with `n = 10M`, since the runtimes are still large enough to have significantly higher performance gains than what we see. The overhead associated with having more threads seems to be quite large, leading to a poor strong scaling for this quicksort implementation.

In contrast, my quicksort implementation saw roughly the same performance gains but continued to get better as the number of threads increased past 12. Where the runtime started to increase with the number of threads, the primary cause was that it was already so low (my code consistently had between 30-100x better performance than the provided implementation for this assignment depending on the array size and the number of threads) that even the relatively low communication I used was inevitably going to outweigh runtime costs.

![Strong scaling graphs](images/quicksort_strong_scaling.png)

#### Question 1.3
In `quicksort.cc` there are hard-coded variables that control the flow of code. Namely

```c++
#define NWS 5e4   /* minimum size for workshare  */
#define NTSK 5e4  /* size to switch to tasks     */
#define NMIN 50   /* minimum size for final task */
```

Describe how these variables produce the effect commented above. Experiment with code by changing these variables.

##### Answer 1.3
The effects of these macros seem to be the following:
- `NWS`: when the size of an array drops below this value, the scanning and swapping steps of pivoting are performed sequentially, rather than in parallel.
- `NTSK`: if the array has size at least `NTSK` then sorting is done in parallel by separating the pivots into OpenMP tasks. If it is below `NTSK`, then running `quicksort` will just do a normal, run-of-the-mill sequential sort.
- `NMIN`: as long as the array size is above this amount, when we recursively sort on the sub-arrays we create one task for sorting each subarray. As a result, even if the array size is larger than `NWS` or `NTSK`, we can still do parallel work by having multiple cores work on different subarrays of the data.

I started by seeing what would happen when I decreased `NWS` and `NTSK` by one order of magnitude. The performance drop in both weak and strong scaling was pretty egregious -- across the board I got worse runtimes for both the weak and strong scaling tests. For small numbers of threads the time loss was small enough that it could reasonably be attributed to noise -- for instance, on an array of size 10M I got a runtime of 10.7s, compared to 9.84s for the original options. However, for large numbers of threads the time costs were crushing: on 24 threads and an array of size 10M, I got a runtime of 8.99s, an 86% increase over the corresponding runtime with the original values of `NWS` and `NTSK`. On the weak scaling tests with 10M items per thread, the runtime of quicksort was *over three minutes* at about 200s, compared with 122s when `NWS = 5e4`  and `NTSK = 5e4`.

After this, I tried putting `NWS` and `NTSK` back to their original values, and instead looked at what happened when I let `NMIN = 1e3`. To me this is a sensible threshold, since it is roughly the threshold at which in my code adding more processors only increased the runtime, since at that point I was sorting in beneath a millisecond. Unfortunately, this change had little impact on runtime. For instance, in the table below the number of array elements to sort per thread is nearly identical to the table shown under Answer 1.3 for the quicksort weak scaling analysis. This seems to suggest that increasing `NMIN` doesn't have much effect on performance (unless `NMIN` is extremely large or small). Similarly, the strong scaling times were only mildly changed, and probably not to an extent that would indicate a runtime increase or decrease.

|          | `n = 1M * p` | `n = 10M * p` |
|----------|--------------|---------------|
| `p = 1`  |    `1.41e+0` |     `1.43e+1` |
| `p = 2`  |    `1.76e+0` |     `1.84e+1` |
| `p = 4`  |    `2.16e+0` |     `2.25e+1` |
| `p = 8`  |    `3.00e+0` |     `2.96e+1` |
| `p = 12` |    `3.85e+0` |     `4.05e+1` |
| `p = 16` |    `5.04e+0` |     `5.18e+1` |
| `p = 20` |    `6.44e+0` |     `6.62e+1` |
| `p = 24` |    `1.18e+1` |     `8.44e+1` |



For my final test, I reset `NMIN` to its original value and *increased* `NWS` and `NTSK`, both to `10e4`. I found that I got a modest performance gain from doing this. For the weak scaling analysis I got the data below:

|          | `n = 1M * p` | `n = 10M * p` |
|----------|--------------|---------------|
| `p = 1`  |    `1.39e+0` |     `1.44e+1` |
| `p = 2`  |    `1.80e+0` |     `1.73e+1` |
| `p = 4`  |    `2.13e+0` |     `2.15e+1` |
| `p = 8`  |    `2.83e+0` |     `2.90e+1` |
| `p = 12` |    `3.67e+0` |     `3.79e+1` |
| `p = 16` |    `4.79e+0` |     `4.88e+1` |
| `p = 20` |    `5.79e+0` |     `6.09e+1` |
| `p = 24` |    `7.11e+0` |     `7.90e+1` |

Almost all of the listed times are smaller than both those in my previous test (with fixed `NWS` and `NTSK` and boosted `NMIN`), which themselves were generally the same as the times with the original macro definitions (or slightly smaller). In general, though, it's hard to tell whether or not the performance gain is really that much. At the upper end of the spectrum it's around a 10% boost (although there's one outlier -- `n = 1M * p`, `p = 24` -- for which the gain is 40%. The presence of this outlier can probably be attributed to noise). In general, though, where gains exist are closer to 5-6%. Overall, I'd conclude that although this change may have improved performance, the improvement isn't that impressive. As was the case with the previous analysis, the strong scaling times are pretty close to the originals, and exhibit the same patter of increasing at around 12 to 16 threads.

#### Question 1.4
In `config.summit.rc` is the following line

```bash
export KMP_AFFINITY="granularity=thread,scatter"
```

Explore the documentation for [thread affinity](https://software.intel.com/en-us/node/522691) to learn more about thread allocation. What is the effect of this variable?

##### Answer 1.4
Thread affinity is the allocation of threads to physical cores. This can improve performance by keeping threads associated with the same resources throughout execution, which can allow them to make better use of cached memory. In OpenMP the `KMP_AFFINITY` variable provides a high-level interface for controlling thread affinity. By specifying `KMP_AFFINITY="granularity=thread,scatter"`, we are telling OpenMP that threads should be spread among the machine's cores as evenly as possible (see [documentation](https://software.intel.com/en-us/node/522691#KMP_AFFINITY_ENVIRONMENT_VARIABLE) for the `KMP_AFFINITY` variable). It then binds those threads to those cores so that a single thread never runs on more than one core.
