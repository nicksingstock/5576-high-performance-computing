/*
 * @input x data vector
 * @input num_bins the number of bins to use
 * @output bin_bdry vector of size num_bins+1 holding the left and
 *         right limit of each bin
 * @output bin_count array of size num_bins holding the count of
 *         items in each bin.
 *
 * Both output vectors are cleared and filled out by the histogram
 * function.
 */

#include <cstddef>
#include <vector>

#include "histogram.h"
#include <omp.h>
#include <stdio.h>
#include <math.h>

typedef std::vector<double> vec_t;
typedef std::vector<size_t> count_t;

void histogram(const vec_t &x, int num_bins,          /* inputs  */
	           vec_t &bin_bdry, count_t &bin_count)  /* outputs */
{

//------------------ sequential section --------------
//get length of x
int len_x = x.size();
double min_bin = x[0], max_bin = x[0];
bin_count.resize(num_bins);
bin_bdry.resize(num_bins+1);

//------------------ parallel section ----------------
// get min x and set it to first bin barrier


#pragma omp parallel for reduction(min:min_bin)
for (int i = 0; i<len_x; i++)
	if (x[i] < min_bin) 
		min_bin = x[i];

bin_bdry[0] = min_bin;


// get max x and set to last bin barrier

#pragma omp parallel for reduction(max:max_bin)
for (int i = 0; i<len_x; i++)
	if (x[i] > max_bin)
		max_bin = x[i];

bin_bdry[num_bins] = max_bin;



//------------------ sequential section --------------
// fill in intermediate barriers
double bin_len = (max_bin - min_bin) / num_bins;

for(int i=1; i<num_bins; i++)
	bin_bdry[i] = bin_bdry[i-1] + bin_len;


//------------------ parallel section ----------------
// add vector data to corresponding bin
double ip = num_bins / (max_bin*(1+1e-14) - min_bin);

#pragma omp parallel for
for(int i=0; i<len_x; i++)
{
	int j = floor( x[i]*ip );

	#pragma omp critical
	bin_count[j] = bin_count[j]+1;
}

// end function
}
