Task 1:
	1. complete!
	2. p = 24 (max allowed), er = 1e-5, ea = 1e-12, kmax = 100
	( tried kmax = 10,000 and not even 256 could finish in time )
	
	Decreasing er to 1e-4 as this works better. There seems to be an issue 
	where optimization occurs for a couple of steps and then all values 
	become 0, this could be datatype related but I'm not sure. Anyways, I
	have decreased kmax to 10.
		
	n = 256: 
	time = 3.34 seconds
	r0 = 67399.8	rf = 1.9783
	steps = 1

        n = 512:
        time = 29.566 seconds
        r0 = 4.17e+6	rf = 43.382
        steps = 4

        n = 758:
        time = 82.03 seconds
        r0 = 4.095e+7	rf = 239.9
        steps = 8

        n = 1024:
        time = 217.2 seconds
        r0 = 4.13e+8	rf = 1017.6
        steps = 10


