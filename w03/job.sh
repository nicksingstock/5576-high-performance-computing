#!/bin/bash

#SBATCH --time=1:00:00
#SBATCH --nodes=1
#SBATCH -o sort_st-%j.out
#SBATCH -e sort_st-%j.err
#SBATCH --ntasks 16
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

ARRAY=100000

OMP_NUM_THREADS=1
echo "1 thread"
./test_quicksort.exe ARRAY

OMP_NUM_THREADS=4
echo "4 threads"
./test_quicksort.exe ARRAY

OMP_NUM_THREADS=16
echo "16 threads"
./test_quicksort.exe ARRAY

OMP_NUM_THREADS=1
echo "1 thread"
./test_quicksort.exe 10*ARRAY

OMP_NUM_THREADS=4
echo "4 threads"
./test_quicksort.exe 10*ARRAY

OMP_NUM_THREADS=16
echo "16 threads"
./test_quicksort.exe 10*ARRAY


