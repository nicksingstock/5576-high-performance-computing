// This implementation originally belongs to Alex Zderic
// Modified by: Keval Shah
// Modified the section that combines the private_bin_count to bin_count

#include "histogram.h"
#include "omp.h"
#include <algorithm>
#include <functional>
#include <iostream>

void histogram(const vec_t &x, int num_bins,            /* inputs */
               vec_t &bin_bdry, count_t &bin_count) {   /* outputs */
    
    // find max and min, these will give bin boundaries
    //// the contents of x are doubles (see header file)
    double min = x[0];
    double max = x[0];
    
    //// loop through x to find max an min with compares (use OpenMP for reduction)
    #pragma omp parallel for reduction(max: max), reduction(min: min)
    for (size_t i=0; i<x.size(); i++) {
        if (x[i] > max) max = x[i];
        if (x[i] < min) min = x[i];
    }

    // make bin_bdry
    //// set the size of bin_bdry
    bin_bdry.reserve(num_bins+1);
    
    //// set the lower bin boundry
    bin_bdry[0] = min*(1-1e-14);
    bin_bdry[num_bins] = max*(1+1e-14);
    
    //// calculate the individual bin size/width
    double bin_size = (bin_bdry[num_bins] - bin_bdry[0]) / num_bins;
    
    //// loop through bin_bdry to assign values to the inner bin boundaries (OpenMP for)
    #pragma omp parallel for
    for (int i=1; i<num_bins; i++) {
        bin_bdry[i] = bin_bdry[0] + i*bin_size;
    }
    
    // make bin_count
    //// allocate bin_count memory and assign values
    bin_count.reserve(num_bins);
    count_t par_bin_count(num_bins);
    
    //// assign values to bin_count
    #pragma omp parallel for
    for (size_t i=0; i<num_bins; i++) {
        bin_count[i] = 0;
        par_bin_count[i] = 0;
    }   
    //// create sequential writing offset variable
    int offset;
    
    //// increment bin_count and par_bin_count
    #pragma omp parallel firstprivate(par_bin_count) private(offset) shared(bin_count)
    {
        //// loop through x and increment each threads private par_bin_count seperately (avoids race condition)
        #pragma omp for nowait
        for (int i=0; i<x.size(); i++) {
            double bin = x[i]/bin_size;
            if (x[i] < bin_bdry[(int)bin]) bin--;
            par_bin_count[(int)bin]++;
        }

        //// calculate the offset for a given thread
        offset = (num_bins*omp_get_thread_num()) / omp_get_max_threads();
        //// combine the par_bin_counts with a for loop, use the offset to avoid the invoking of the atomic pragma
        for (int i=0; i<num_bins; i++)bin_count[(i+offset)%num_bins] += par_bin_count[(i+offset)%num_bins];
    }
}
