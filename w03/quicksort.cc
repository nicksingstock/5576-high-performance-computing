/**
 * @file
 * @author Nick Singstock
 * @revision $Revision: 1 $
 * @tags $Tags: tip $
 * @date $Date: Tue Sep 26 18:22:46 2017 -0600 $
 *
 * @brief quicksort header
 */

#include <vector>
#include <omp.h>

#include "quicksort.h"

typedef std::vector<long> vec_t;

void swap(int j, vec_t &y)
{
if (y[j]>y[j+1]){
int dummy = y[j];
y[j] = y[j+1];
y[j+1] = dummy;
}
}

void quicksort(const vec_t &x, vec_t &y)
{
// get length of x
long len = x.size();

// assign x values to y
#pragma omp parallel for
for(int k=0; k<len; ++k)
	y[k] = x[k];

// define phase
int phase = 0;
// itterate through entire array
for(int i=0; i<len; ++i)
{
phase = (phase+1)%2; // can only be 0 or 1

// swap values in parallel using tasks
for(int j=phase; j<len; j+=2)
{
#pragma omp task shared(y)
swap(j,y);
}
#pragma omp taskwait


}// end for
}// end function
