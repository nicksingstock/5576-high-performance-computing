#!/bin/bash

#SBATCH --time=0:01:00
#SBATCH --nodes=1
#SBATCH -o pc-%j.out
#SBATCH -e pc-%j.err
#SBATCH --ntasks 2
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

OMP_NUM_THREADS=2
echo "1 thread"
./pc.exe 10 1 1
