/*
  Call function with: ./transpose.exe X Y Z
  Where X is the number of rows, Y is columns
  and Z is the number of random points to generate in the sparse matrix

  CSR transpose code by Nick Singstock 10/31
*/

#include <iostream>
#include <mpi.h>
#include <map>
#include <string>
#include <vector>
#include <cstdlib>
#include <time.h>
#include <chrono>

typedef double data_t;

int main(int argc, char** argv){

  // get commandline arguments
  if (argc<3){
    printf("Necessary arugments m and n (matrix size) not received for %s.\n", __FILE__);
    exit(1);
  }
  if (argc>4){
    printf("Too many arugments received for %s (max 3).\n", __FILE__);
    exit(1);
  }

  // Get m and n from command line and decide how many CSR points to make.
  int m(atoi(argv[1]));
  int n(atoi(argv[2]));
  int points = 10;
  if (argc>3)
    points = atoi(argv[3]);

  // initialize CSR values
  std::vector<int> col( points );
  std::vector<int> row( points );
  std::vector<double> values( points );

  // Initialize MPI
  MPI_Status status;
  MPI_Init(&argc, &argv);
  
  // get processor rank
  int rank, nproc;
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // assign division arrays
  std::vector<int> div( 2 );
  std::vector<int> assign( nproc*2 );
  std::vector<int> displs( nproc );
  std::vector<int> cnt( nproc );

  // start timer
  std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();


  // if I'm the root processor
  if (rank == 0){
    for (int ii=0; ii < points; ii++)
      {
	col[ii] = rand() % n;
	row[ii] = rand() % m;
	values[ii] = (rand() % (10*points)) / points;
	}

    std::cout<<"Initialized CSR with "<<points<<" random sparse values and dimensions (m,n) = "<<m<<", "<<n<<std::endl;
    
    // decide labor division
    int start=0; //inclusive
    int end, disp; //exclusive
    for (int ii=0;ii<nproc;ii++)
      {
	displs[ii]=start;
	disp = ((points-(points%nproc))/nproc); 
	  if (ii<(points%nproc)) disp++;
	end = start + disp;
	assign[2*ii] = start;
	assign[2*ii+1] = end;
	start = end;
	cnt[ii]=disp;
      }    
  }
  
  // Assign rows
  MPI_Scatter( &assign[0],2,MPI_INT, &div[0],2,MPI_INT,0,MPI_COMM_WORLD );
  
  //std::cout<<"Div received: "<<div[0]<<", "<<div[1]<<std::endl;

  // broadcast values
  MPI_Bcast( &col[0], col.size(), MPI_INT, 0, MPI_COMM_WORLD );
  MPI_Bcast( &row[0], row.size(), MPI_INT, 0, MPI_COMM_WORLD );
  MPI_Bcast( &values[0], values.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD );
  MPI_Bcast( &cnt[0], cnt.size(), MPI_INT, 0, MPI_COMM_WORLD );
  MPI_Bcast( &displs[0], displs.size(), MPI_INT, 0, MPI_COMM_WORLD );
  
  // transpose matrix based on assigned division of points
  int jobs = div[1]-div[0];
  std::vector<int> colt( points );
  std::vector<int> rowt( points );
  std::vector<int> subcolt( jobs );
  std::vector<int> subrowt( jobs );
  for (int j = 0; j<jobs; j++)
    {
      subcolt[j]=row[j];
      subrowt[j]=col[j];
    }
  //std::cout<<"rank, jobs, cnt, disp: "<<rank<<" "<<jobs<<" "<<cnt[rank]<<" "<<displs[rank]<<std::endl;
  
  // Gather all new values together
  MPI_Gatherv( &subcolt[0], jobs, MPI_INT, 
	       &colt[0], &cnt[0], &displs[0], 
	       MPI_INT, 0, MPI_COMM_WORLD );
  MPI_Gatherv( &subrowt[0], jobs, MPI_INT,
               &rowt[0], &cnt[0], &displs[0],
               MPI_INT, 0, MPI_COMM_WORLD );

  // print time elapsed
  std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> time_span = end - start;

  if (rank==0)
    {
      std::cout<<"\nTranspose Successful! \nElapsed time: "<< time_span.count() <<" seconds"<<std::endl;
      std::cout<<"First data point of CSR: ("<<row[0]<<", "<<col[0]<<") | Value: "<<values[0]<<std::endl;
      std::cout<<"After tranpose: ("<<rowt[0]<<", "<<colt[0]<<") | Value: "<<values[0]<<"\n\n"<<std::endl;
    }

  // end
  MPI_Finalize();
  return 0;
}
