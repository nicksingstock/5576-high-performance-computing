/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision$
 * @tags $Tags$
 * @date $Date$
 *
 * @brief
 */

#ifndef _SEMAPHORE_H_
#define _SEMAPHORE_H_

#include <omp.h>
#include <string>

class Sem
{
  public:

    // Initialize a semaphore object and gives it an initial integer
    // value.
    Sem(int val, const char *name);

    // Tidy up the semaphore when we are finished with it.
    ~Sem();

    // Increase the value of the semaphore by 1.
    void post();

    // Atomically decreases the value of the semaphore by 1, but
    // always waits until the semaphore has a nonzero count
    // first. Therefore, we call `sem_wait` on a semaphore with a
    // value of 2, the thread decrements the semaphore to 1 and
    // returns. If we call it on a semaphore with a value of 0, the
    // function call blocks until some other thread has incremented
    // the value so that it is no longer 0. If two thread are both
    // waiting in `sem_wait` for the same semaphore to be nonzero and
    // it is incremented once by a third process, only one of the two
    // waiting process will get to decrement the semaphore and
    // continue while the other will remain waiting.
    void wait();

  private:
    int init_, val_;
    std::string name_;
    omp_lock_t val_lock_;
    omp_lock_t zero_lock_;
};

#endif //_SEMAPHORE_H_
