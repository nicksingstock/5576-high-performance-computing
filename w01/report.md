TASK 1:
1:     PATH   The  search  path  for  commands.   It is a colon-separated list of directories in which the shell looks for commands (see COMMAND EXECUTION below).  A zero-length (null)
       directory name in the value of PATH indicates the current directory.  A null directory name may appear as two adjacent colons, or as an initial or  trailing  colon.   The
       may use file names with tildes in assignments to PATH, MAILPATH, and CDPATH, and the shell assigns the expanded value.
2:     LD_LIBRARY_PATH only occurs once, the entry is:
         5.  For a native linker, search the contents of the environment variable "LD_LIBRARY_PATH".

3:     /home/nisi6161

TASK 2:
4. Done
5. 
prepend_path("PATH","/curc/sw/gcc/5.4.0/bin")
prepend_path("LD_LIBRARY_PATH","/curc/sw/gcc/5.4.0/lib64")
prepend_path("PATH","/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/bin/intel64")
prepend_path("LD_LIBRARY_PATH","/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/intel64")

6.  MPI implementations and compiler dependent applications are now available
7.  If intel wasnot previosuly loaded, but one of its dependencies are, then those
    dependent modules will not work properly 

TASK 3:
8. Intel:
	setenv("CURC_INTEL_ROOT","/curc/sw/intel/17.4")
	setenv("CURC_INTEL_LIB","/curc/sw/intel/17.4/lib")
	setenv("CURC_INTEL_INC","/curc/sw/intel/17.4/include")
	setenv("CURC_INTEL_BIN","/curc/sw/intel/17.4/bin")
   	setenv("INTEL_LICENSE_FILE","/curc/sw/intel/17.4/licenses/USE_SERVER.lic")
	pushenv("CC","icc")
	pushenv("FC","ifort")
	pushenv("CXX","icpc")
	pushenv("AR","xiar")
	pushenv("LD","xild")

    gcc:
	setenv("CURC_GCC_ROOT","/curc/sw/gcc/6.1.0")
	setenv("CURC_GCC_LIB","/curc/sw/gcc/6.1.0/lib")
	setenv("CURC_GCC_INC","/curc/sw/gcc/6.1.0/include")
	setenv("CURC_GCC_BIN","/curc/sw/gcc/6.1.0/bin")
	pushenv("CC","gcc")
	pushenv("FC","gfortran")
	pushenv("CXX","g++")

TASK 4:
9.  Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/curc/sw/gcc/6.1.0/libexec/gcc/x86_64-pc-linux-gnu/6.1.0/lto-wrapper
Target: x86_64-pc-linux-gnu
Configured with: ../gcc-6.1.0/configure --prefix=/curc/sw/gcc/6.1.0 --enable-languages=c,c++,fortran,go --disable-multilib --with-tune=intel
Thread model: posix
gcc version 6.1.0 (GCC)

TASK 5:	
10. I created the axpy.cc code using: cp axpy.h axpy.cc
    I compiled the axpy.cc code on a compile node using: gcc axpy.cc -c 
    and it made an unreadable axpy.o file
    I don't know how to link this to text_axpy.cc but I am trying to figure it out
	attempt 1: gcc -o text_axpy.exe test_axpy.cc
		did not work
	attempt 2: gcc -o axpy.exe axpy.cc
		did not work
	attempt 3: gcc test_axpy.cc
		did not work
	did not work due to C++ dependencies
   I recompiled axpy.cc (after adding a code for the function) using g++ -c axpy.cc and made axpy.o
   I compiled test_axpy.o using: g++ -c test_axpy.cc
   I linked the two files using: g++ axpy.o test_axpy.o -o test_axpy.exe

11.  I used: squeue -u $USER   to see the status on my job 
     and nano axpy-%j.out to view the output file, which says that it passed

TASK 6:
12.  -fopenmp needs to be added to the compile line
     when running job.sh, an elapsed time is given 

13.  	1: 0.488s
	2: 0.257s
	4: 0.137s
	8: 0.079s
	16: 0.041s

