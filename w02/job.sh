#!/bin/bash

#SBATCH --time=1:00:00
#SBATCH --nodes=1
#SBATCH -o hist_b4_and_outliers_3.out
#SBATCH -e hist.err
#SBATCH --ntasks 16
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel




echo "4 threads"
OMP_NUM_THREADS=4
./test_histogram.exe 9 1 10

echo "16 threads"
OMP_NUM_THREADS=16
./test_histogram.exe 9 1 10

echo "1 thread"
OMP_NUM_THREADS=1
./test_histogram.exe 2 3 1000

echo "4 threads"
OMP_NUM_THREADS=4
./test_histogram.exe 2 3 1000

echo "16 threads"
OMP_NUM_THREADS=16
./test_histogram.exe 2 3 1000

echo "1 thread"
OMP_NUM_THREADS=1
./test_histogram.exe 5 3 1000

echo "4 threads"
OMP_NUM_THREADS=4
./test_histogram.exe 5 3 1000

echo "16 threads"
OMP_NUM_THREADS=16
./test_histogram.exe 5 3 1000

echo "1 thread"
OMP_NUM_THREADS=1
./test_histogram.exe 9 3 1000

echo "4 threads"
OMP_NUM_THREADS=4
./test_histogram.exe 9 3 1000

echo "16 threads"
OMP_NUM_THREADS=16
./test_histogram.exe 9 3 1000

echo "1 thread"
OMP_NUM_THREADS=1
./test_histogram.exe 2 4 10000

echo "4 threads"
OMP_NUM_THREADS=4
./test_histogram.exe 2 4 10000

echo "16 threads"
OMP_NUM_THREADS=16
./test_histogram.exe 2 4 10000

echo "1 thread"
OMP_NUM_THREADS=1
./test_histogram.exe 5 4 10000

echo "4 threads"
OMP_NUM_THREADS=4
./test_histogram.exe 5 4 10000

echo "16 threads"
OMP_NUM_THREADS=16
./test_histogram.exe 5 4 10000

echo "1 thread"
OMP_NUM_THREADS=1
./test_histogram.exe 9 4 10000

echo "4 threads"
OMP_NUM_THREADS=4
./test_histogram.exe 9 4 10000

echo "16 threads"
OMP_NUM_THREADS=16
./test_histogram.exe 9 4 10000

