/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 11 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief
 */

#include "semaphore.h"
#include "omp-utils.h"
#include <cassert>

Sem::Sem(int val, const char *name) :
    init_(val),
    val_(val),
    name_(name)
{
}

Sem::~Sem(){
    assert(val_==init_);
}

// Sem empty(BSZ, "empty");
// Sem filled(0, "filled";

void Sem::post(){
if (name_=="filled")
	val_++;
if (name_=="empty")
	val_--;
}

void Sem::wait(){
if (name_=="empty")
	while(val_==0){}
if (name_=="filled")
	while(val_==5){}
}
