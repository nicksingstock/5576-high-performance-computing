% Residual scaling for checking correctness
%{
n_points = [4,16,32,64,128,256,512];
res = [243.4,19.36,2.040,0.1871,0.01665,0.001473,0.0001301];

semilogy(n_points, res, 'LineWidth', 2);
xlabel('Number of discretization points');
ylabel('Total residual norm');
title('Residual norm scaling for known solution');
grid on
grid minor
%}

% Strong scaling for checking performance (PART ONE)
%{
p = [1,8,27];
t = [5.64,1.54,0.484];

spdup = zeros([length(p) 1]);
for i=1:1:length(p)
    spdup(i) = t(1) / t(i);
end
yyaxis left
plot(p, t, 'LineWidth', 2);
ylabel('Total wall-time (s)');
yyaxis right
plot(p, spdup, 'LineWidth', 2);
ylabel('Speedup');
xlabel('Number of threads');
title('Strong scaling for n = 240 and p = \{1,8,27\}');
grid on
grid minor
%}

% Strong scaling for checking performance (PART TWO)
%{
p = [1,4,9,16,25];
t = [5.61,2.77,1.39,0.808,0.512];

spdup = zeros([length(p) 1]);
for i=1:1:length(p)
    spdup(i) = t(1) / t(i);
end
yyaxis left
plot(p, t, 'LineWidth', 2);
ylabel('Total wall-time (s)');
yyaxis right
plot(p, spdup, 'LineWidth', 2);
ylabel('Speedup');
xlabel('Number of threads');
title('Strong scaling for n = 240 and p = \{1,4,9,16,25\}');
grid on
grid minor
%}

% Weak scaling for checking communication costs

p = [8,27,64];
t = [24.87,25.28,26.56];

plot(p, t, 'LineWidth', 2);
ylabel('Total wall-time (s)');
xlabel('Number of threads');
title('Weak scaling for n^3/p = 27mil and p = \{8,27,64\}');
ylim([0 35]);
grid on
grid minor
