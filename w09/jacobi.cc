/**
 * @author Rahimian, Abtin <arahimian@acm.org>
 *  * @revision $Revision: 25 $
 *   * @tags $Tags: tip $
 * :q   * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *     *
 *      * @brief test driver for Possion
 *       */

#include "poisson.h"
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <cmath>
#include <chrono>
#define PI 3.1415

int main(int argc, char** argv){

    /** add initialization, get n from command line */

  // start timer
  std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
  
  MPI_Init(&argc, &argv);

  if(argc < 2)
    {
      printf("Enter n along each axis\n");
    }

  int n = atoi(argv[1]);

  MPI_Comm grid_comm;
  grid_t x; 
   
  poisson_setup(MPI_COMM_WORLD, n, grid_comm, x);
  
   
  int rank;   
    
  MPI_Comm_rank(grid_comm, &rank);  
  
  
  // do this for every u/v value
  vec_t a, rhs, v;

  for(int i = 0; i < x.size(); i++)
    {   
      double x1 = x[i].x;
      double y1 = x[i].y;
      double z1 = x[i].z;
      double a_val = 12;

      double v_val = sin(x1*4*PI)*sin(y1*12*PI)*sin(z1*8*PI);
      double rhs_val = sin(x1*2*PI)*sin(y1*12*PI)*sin(z1*8*PI);
      if(x1==n-1||y1==n-1||z1==n-1)
	v_val = 0;

      a.push_back(a_val);
      v.push_back(v_val);
      rhs.push_back(rhs_val);
    }

  int steps = 0;
  int k_max = 5;

  matvec_t mv = std::bind(poisson_matvec, std::ref(grid_comm), n, std::ref(a),
			  std::placeholders::_1, std::placeholders::_2);

  vec_t res_0;
  real_t res_norm_0;
  residual(MPI_COMM_WORLD, mv, v, rhs, res_0, res_norm_0);

  double ea = 1e-8;
  double er = 1e-4;

  vec_t res;
  real_t res_norm;
  int length = std::cbrt(v.size());
  
  vec_t u;
  for (int i =0; i<v.size(); i++)
    {
      u.push_back(v[i]);
    }


  while (steps < k_max)
    {
      matvec_t mv = std::bind(poisson_matvec, std::ref(grid_comm), n, std::ref(a),
			      std::placeholders::_1, std::placeholders::_2);
      
      residual(MPI_COMM_WORLD, mv, u, rhs, res, res_norm);
  
      if(res_norm < er*res_norm_0+ea ) {
	break;
      }

      steps = steps +1;
      // if above residual, continue to get D=diag(L), and v+=D-1[r+Dv]
      vec_t D, D_inv;
      double h = 1/(n-1);
      for (int i=0; i< u.size(); i++)
	{
	  if ( floor(i/length) == i%length && i%length == i%(length*length) )
	    {
	      D.push_back( 12-h*h/6 );
	      D_inv.push_back( 1/(12-h*h/6) ); 
	    }
	  else
	    {
	      D.push_back( 0 );
	      D_inv.push_back( 0 );
	    }
	  double place = D_inv[i]*(res[i] + D[i]*u[i]);
	  u[i] = place;
	}
    }
  
  if(rank==0){

    // print time elapsed
    std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span = end - start;

    std::cout<<"Number of Steps: "<<steps<<"\nr0 = "<<res_norm_0<<"\nr = "<<res_norm<<std::endl;
    
    std::cout<<"\nElapsed time: "<< time_span.count() <<" seconds"<<std::endl;
  }		
  
  MPI_Finalize();
  
  return 0;
}


