#!/bin/bash

#SBATCH --time=0:25:00
#SBATCH --nodes=1
#SBATCH -o axpy-%j.out
#SBATCH -e axpy-%j.err
#SBATCH --ntasks 8
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

echo "n = 256"
./jacobi.exe 256

echo "n = 512"
./jacobi.exe 512

echo "n = 758"
./jacobi.exe 758

echo "n = 1024"
./jacobi.exe 1024
