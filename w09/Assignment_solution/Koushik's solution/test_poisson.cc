/**
 * @author Rahimian, Abtin <arahimian@acm.org>
 *  * @revision $Revision: 25 $
 *   * @tags $Tags: tip $
 * :q   * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *     *
 *      * @brief test driver for Possion
 *       */

#include "poisson.h"
#include <iostream>
#include <stdio.h>
#include <math.h>
#define PI 3.1415

int main(int argc, char** argv){

    /** add initialization, get n from command line */

double t1, t2; 
    t1= MPI_Wtime();

    MPI_Init(&argc, &argv);

    if(argc < 2)
    {
        printf("Enter n along each axis\n");
    }

    int n = atoi(argv[1]);

   MPI_Comm grid_comm;
   grid_t x; 
   
   poisson_setup(MPI_COMM_WORLD, n, grid_comm, x);
   
   int rank;   
    
    MPI_Comm_rank(grid_comm, &rank);  

    vec_t a, rhs, v;

    for(int i = 0; i < x.size(); i++)
    {   
        double x1 = x[i].x;
        double y1 = x[i].y;
        double z1 = x[i].z;
        double a_val = 12;
        
        double v_val = sin(x1*4*PI)*sin(y1*10*PI)*sin(z1*14*PI);
        double rhs_val = sin(x1*4*PI)*sin(y1*10*PI)*sin(z1*14*PI)*(12 + 312*PI*PI);
       if(x1==n-1||y1==n-1||z1==n-1)
          v_val = 0;

        a.push_back(a_val);
        v.push_back(v_val);
        rhs.push_back(rhs_val);
    //    printf("Rank %d x %f y %f z %f",rank,x1,y1,z1);
    }

    matvec_t mv = std::bind(poisson_matvec, std::ref(grid_comm), n, std::ref(a),
                            std::placeholders::_1, std::placeholders::_2);
    vec_t res;
    real_t res_norm;
    residual(MPI_COMM_WORLD, mv, v, rhs, res, res_norm);
    
    if(rank==0){
     printf("The residual is %f ", res_norm);
     printf("The residual norm is %f", res_norm/(n*n*n));
    t2= MPI_Wtime();  
    printf("Elapsed=%5.2e\n",t2-t1);
	}		
    MPI_Finalize();

    return 0;
}


