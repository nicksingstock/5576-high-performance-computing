Kelly Kochanski
Kelly Kochanski report - w06

***Task 1***

**1a) The histogram code**

The given code, histogram.cc, puts an array *x* of numbers into a histogram, then it returns the bin boundaries and the number of items in each bin.
In parallel, the threads work together to set up the histogram (bin boundaries, max and min of the data, etc), then they each make a local count of the number of items in each bin from a limited part of the dataset, then add all of the local counts together to make the final histogram.

My code is very structurally similar.
Both threads start the parallel region inside of the histogram() function, and use a for reduction to calculate the min and max values of the data (though the new code uses more concise syntax for the for reduction).
After the for reduction, the new code uses #pragma omp flush(mn, mx). This seems like a cool way to make those values more thread safe.
Both codes use parallel fors to find bin boundaries and set initial bin counts to zero. Both codes also make local counts, one per thread, of histogram items, then sum them up.
In my code, I do the sum using either a for reduction or a regular parallel for loop depending on the number of bins; the new code uses only the parallel for loop.

**1b) The quicksort code**

Again, the new quicksort.cc is structurally similar to mine: quicksort spins off recursive tasks (called "quicksort_tsk" in the new code and "quicksort_helper" in mine). These recursive tasks call a function called "partition" on a sub-array. Partition arranges the elements of the sub-array around a pivot, using a scan ("parallel_scan_iterative" in my code, "scan_par" in new code) to identify where each element should go.

The details of the implementations, however, are very different.
First, the parallelization in my code exists entirely within each recursive call to quicksort_helper/quicksort_tsk, whereas the new code spins each iteration of quicksort_tsk up as a new parallel task.
I think the new approach is better: this should lead to efficient parallelism once the code is several layers of recursion deep and many copies of quicksort_tsk are running at once.

Second, the parallel scans are implemented differently. My scan is serial with calls to parallel for; the new scan is mostly inside a single parallel region with calls to barrier. Maybe the new version is faster, if the barrier costs less time than opening and closing new parallel regions.

I believe that both codes have the same structure for functions quicksort(...) and quicksort_helper(...)/quicksort_tsk(...), though with different variable names.

My code has more comments and a sprinkling of asserts (those probably slow it down, but they're useful for debugging and easy to turn off).

**2a) Histogram scaling**

The scaling for the histogram is below. All results are averaged over three runs. The bump at 4 processors (in the n=9 categories is probably erroneous; the run on quartz timed out and I restarted it at that point.

Runs with outliers are dashed lines; runs without outliers are solid. The legend shows the log arguments of the histogram (so n=2 means 10^2=100 items were sorted into bins).

![histogram scaling](hist_scaling.png)

We see that small arrays (n=2) are never efficient to sort in parallel.

For larger numbers of processors, the time seems to be influenced by a combination, or perhaps the larger, of nbins and n_elements.

The outliers do not have a systematic effect on the results. However, while I was doing the runs I did notice that they increased the variability of the run-time considerably.

**2b) Quicksort scaling**

Strong scaling: parallelism speeds the code up, for problem sizes of both 1M and 10M, until there are 24 cores.

![strong scaling quicksort](quicksort_strong_scaling.png)

Weak scaling:

![weak scaling quicksort](quicksort_weak_scaling.png)


**3) Hard-coded variables in quicksort.cc**

Quicksort.cc has three hard-coded variables.

*NWS* controls how the scan is done. If the size of the array to be sorted is bigger than NWS, then it is scanned in parallel; if not, it is simply summed up sequentially.

*NTSK* spins small quicksort arrays (smaller than NTSK) as separate parallel tasks, with nowait so that other parts of the array can be sorted.

*NMIN* says that quicksort tasks should only be done in parallel if they are bigger than NTSK - presumably, the idea is that if an array is smaller than NTSK, it is quicker to sort it than to start up a new task.

I ran a mini sensitivity experiment to test the values of these parameters.
For the experiment, I used n=10^5 and nbins=10^3 on 4 threads, because this case had a good parallel speedup, and therefore it might actually be useful to parallelize it (unlike, say, running n=100 on 20 cores, which is clearly not an efficient way to solve the problem). At each step, the variable I varied is italicised for emphasis.

(Each of these is averaged over ten runs. I tested the variability of the timing between runs, and found that the uncertainty on the mean time of ten runs was 0.3ms. So, the differences in the table below are mostly significant.)

NWS(x10^4)	|		NTSK(x10^4)		|		NMIN		| Time (ms)
---			|		-----		|	-----			|	-----
5.0			|	5.0				|	50				|	14.7
*5.5*			|	5.0				|	50				|	14.3
*4.5*			|	5.0				|	50				|	12.8
5.0			|	*5.5*				|	50				|	15.1
5.0			|	*4.5*				|	50				|	13.6
5.0			|	5.0					|	*55*			|	14.1
5.0			|	5.0					|	*45*			|	12.9

I don't get any really dramatic differences by changing those parameters, but on this problem size, it looks like I could optimize the algorithm by decreasing NWS and NMIN : in short, it's worth parallelizing smaller problems than are currently parallelized.	


**4) thread affinity on summit**

Thread affinity controls the connections between OpenMP threads and physical processing units, such as cores on Summit.

On summit, the affinity is controlled by:

    $export KMP_AFFINITY="granularity=thread,scatter"

Setting "granularity=thread" requests the finest granularity level: it binds each thread to a single processor. In contrast, setting "granularity=core" (the broadest granularity level, and the default if none is set on the system) allows many threads to be bound to one core, which then floats between different thread contexts.

Using thread affinity of type "scatter" distributes threads as evenly as possible across the system. So, for example, if several nodes are available then the system will endeavor to put threads "n" and "n+1" on different nodes. The opposite of this is "compact", which keeps nodes "n" and "n+1" as close together as possible.


I assume that some programs work best with different thread settings. For example, an MPI program where messages are often passed between nodes with neighboring numbers might be faster if the thread affinity is "compact" so the messages stay within one node. On the other hand, if the load is balanced such that, say, threads 0 and 1 often do work at the same time, then a "scattered" affinity might balance the load better, especially if the system also has "granularity=core" so closely-placed threads share cores and computing resources. 
