/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision$
 * @tags $Tags$
 * @date $Date$
 */

#include <iostream>
#include <mpi.h>

int main(int argc, char** argv){

    MPI_Init(&argc,&argv);
    int rank,nproc;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Status st;
    if (rank == 0) {
    int a = 123;
    MPI_Send(&a, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
    } else if (rank == 1) {
    int b;
    MPI_Recv(&b, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &st);
    std::cout<<"Process 1 received number "<<b<<" from process 0\n";
    }

    MPI_Finalize();
}
