#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH --nodes=1
#SBATCH -o axpy-%j.out
#SBATCH -e axpy-%j.err
#SBATCH --ntasks 24
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

echo "Processors = 24"
mpiexec -n 24 ./transpose.exe 1000000 1000000 5000000

