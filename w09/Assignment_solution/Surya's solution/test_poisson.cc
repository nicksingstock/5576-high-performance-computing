
#include "poisson.h"
#include <iostream>
#include <math.h>
#include <stdio.h>
#define PI 3.1415

int main(int argc, char** argv){

    MPI_Init(&argc, &argv);


    if (argc < 2) {
        printf("Enter the number of items along each axis.\n");
        return 1;
    }

    MPI_Comm grid_comm;
    int n = atoi(argv[1]);
    grid_t x;

    double t1, t2; 
    t1= MPI_Wtime(); 

    poisson_setup(MPI_COMM_WORLD, n, grid_comm, x);

    vec_t a, v, rhs, res;
    real_t res_norm;

    matvec_t mv = std::bind(poisson_matvec, std::ref(grid_comm), n, std::ref(a),std::placeholders::_1, std::placeholders::_2);

    for(int i = 0; i < x.size(); i++) 
    {    
        a.push_back(12);
        real_t value= sin(4*PI*x[i].x)*sin(10*PI*x[i].y)*sin(14*PI*x[i].z);
        v.push_back(value); 
        rhs.push_back(value*(12 + 312*PI*PI)); 
    } 


    int rank;   
    MPI_Comm_rank(grid_comm, &rank);  
    res.reserve(v.size()); 

    residual(MPI_COMM_WORLD, mv, v, rhs, res, res_norm);
     
    if(rank==0){
        res_norm = (sqrt(res_norm))/(n*n*n);
        std::cout<<"Residual norm: "<<res_norm<<std::endl; 
        t2= MPI_Wtime(); 
        printf("Elapsed=%5.2e\n",t2-t1);
    }            

    MPI_Finalize();

    return 0;
}


