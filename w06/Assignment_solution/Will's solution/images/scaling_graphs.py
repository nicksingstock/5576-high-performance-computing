#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats

### Weak scaling plots for quicksort
# Abtin's algorithm
threads    = [1, 2, 4, 8, 12, 16, 20, 24]
t_1M       = [1.41, 1.88, 2.41, 2.97, 3.81, 4.99, 6.42, 11.8]
t_10M      = [14.4, 19.4, 24.2, 31.4, 38.8, 50.8, 63.3, 122.0]

# Will's algorithm
threads2 = [1, 2, 4, 8, 16]
t2_1M  = [4.89e-2, 5.18e-2, 6.68e-2, 8.57e-2, 1.13e-1]
t2_10M = [4.93e-1, 5.30e-1, 6.50e-1, 8.71e-1, 1.26e+0]

# Exponential regression on Abtin's runtimes
slope_10M, intercept_10M, r_value_10M, p_value, std_err = stats.linregress(threads, np.log(t_10M))
slope_1M, intercept_1M, r_value_1M, p_value, std_err = stats.linregress(threads, np.log(t_1M))

# Exponential regression on Will's runtimes
slope2_1M, intercept2_1M, r_value2_1M, p_value, std_err = stats.linregress(threads2, np.log(t2_1M))
slope2_10M, intercept2_10M, r_value2_10M, p_value, std_err = stats.linregress(threads2, np.log(t2_10M))

'''
print( "1M:", np.exp(slope_1M), r_value_1M )
print( "10M:", np.exp(slope_10M), r_value_10M )
print( "1M (mine):", np.exp(slope2_1M), r_value_1M )
print( "10M (mine):", np.exp(slope2_10M), r_value_10M )

fit_1M = np.exp( slope_1M * np.linspace(0, 24, num=100) + intercept_1M )

fig, ax = plt.subplots(1, 2)
ax[0].plot( threads, t_1M  )
ax[1].plot( threads, t_10M )

ax[0].set_ylabel( "Runtime (seconds)" )
ax[0].set_xlabel( "Number of threads" )
ax[0].set_title( "Quicksort weak scaling (1M elements/thread)" )
ax[1].set_xlabel( "Number of threads" )
ax[1].set_title( "Quicksort weak scaling (10M elements/thread)" )

plt.show()

# Strong scaling plots for quicksort
t_1M  = [1.41e+0, 9.50e-1, 5.85e-1, 3.90e-1, 2.80e-1, 3.14e-1, 3.10e-1, 4.64e-1]
t_10M = [1.44e+1, 9.84e+0, 6.08e+0, 3.87e+0, 3.04e+0, 3.12e+0, 3.15e+0, 4.83e+0]

fig, ax = plt.subplots(1, 2)
ax[0].plot( threads, t_1M  )
ax[1].plot( threads, t_10M )

ax[0].set_ylabel( "Runtime (seconds)" )
ax[0].set_xlabel( "Number of threads" )
ax[0].set_title( "Quicksort strong scaling (1M elements)" )
ax[1].set_xlabel( "Number of threads" )
ax[1].set_title( "Quicksort weak scaling (10M elements)" )

plt.show()
'''

### Strong scaling plots for histogram
# n = 1e4 bins

# Times for m = 1e5 array elements, no outlier
t5 = np.array([1.80e-3, 1.17e-3, 9.35e-4, 8.82e-4, 9.88e-4, 1.16e-3, 1.29e-3, 3.98e-3])
t9 = np.array([1.52e+1, 7.97e+0, 3.95e+0, 2.29e+0, 1.61e+0, 1.39e+0, 1.02e+0, 9.47e-1])



# Times for m = 1e9 array elements, no outlier
#slope, intercept, r_value, p_value, std_err = stats.linregress(threads, np.log(t9))
#print( np.exp(slope), r_value )
slope, intercept, r_value, p_value, std_err = stats.linregress(threads, 1 / t9)
print( slope, intercept, r_value )
print( "Runtime gain by adding one thread: between ",
        1 - 1 / (2 + intercept/slope), "(t = 1),",
        1 - 1 / (13 + intercept/slope), "(t = 12), and",
        1 - 1 / (25 + intercept / slope), "(t = 24)" )
print( "Runtime gain by doubling the number of threads: between ",
        1 - 1 / (2 + intercept/(1 *slope)), "(t = 1),",
        1 - 1 / (2 + intercept/(4 *slope)), "(t = 4), and",
        1 - 1 / (2 + intercept/(24*slope)), "(t = 24)" )


fig, ax = plt.subplots(1, 2)
ax[0].plot( threads, t5 )
ax[1].plot( threads, t9 )
ax[1].plot( np.linspace(0, 24, num=200), 1 / (intercept + slope*np.linspace(0, 24, num=200)), "k:" )
#ax[1].plot( np.linspace(0, 24, num=200), np.exp(intercept + slope*np.linspace(0, 24, num=200)), "k:" )
ax[0].set_ylabel( "Runtime (seconds)" )
ax[0].set_xlabel( "Number of threads" )
ax[0].set_title( "Histogram strong scaling (1e5 elements)" )
ax[1].set_xlabel( "Number of threads" )
ax[1].set_title( "Histogram strong scaling (1e9 elements)" )

plt.show()
