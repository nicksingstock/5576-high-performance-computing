#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH --nodes=1
#SBATCH -o axpy-%j.out
#SBATCH -e axpy-%j.err
#SBATCH --ntasks 24
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

./test_poisson.exe
