#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH --nodes=1
#SBATCH -o sort_16-%j.out
#SBATCH -e sort_16-%j.err
#SBATCH --ntasks 16
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel


OMP_NUM_THREADS=16
echo "weak"
./test_quicksort.exe 1000000

OMP_NUM_THREADS=16
echo "strong"
./test_quicksort.exe 1600000

OMP_NUM_THREADS=16
echo "strong"
./test_quicksort.exe 16000000

