/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 */

#include "poisson.h"
#include <iostream>
#include <mpi.h>
#include <vector>


void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm,grid_t &x)
{
    std::cout<<"setup"<<std::endl;

    int argc; 
    char **argv;
    MPI_Init(&argc, &argv);

    std::cout<<"Initialized"<<std::endl;
    int rank, nproc;
    // MPI_COMM_WORLD
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    std::cout<<"create dimensions"<<std::endl;
    // Create Dimensions
    int dims[3];
    MPI_Dims_create(nproc, 3, 0);
    int const period[3] = {0,0,0};
    // period[0]=0;period[1]=0;period[2]=0;

    std::cout<<"create cart"<<std::endl;
    // Create Cartesian
    MPI_Cart_create(comm, 3, dims, period, 1, &grid_comm);
    
    std::cout<<"create coords"<<std::endl;
    //int coords[3];
    // Get coordinates
    MPI_Cart_coords(grid_comm, rank, 3, x[rank].coord);

    std::cout<<"finalize setup"<<std::endl;
    MPI_Finalize();
}

void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, const vec_t &v, vec_t &lv)
{
    std::cout<<"matvec"<<std::endl;
    int length = a.size();
    int width = v.size();
    for (int i=0; i<length; i++){
	for (int j=0; j<width; j++){
	lv[i,j] = a[i]*v[i,j];
	}
    }
}

void residual(const MPI_Comm &comm, matvec_t &mv, const vec_t &v, const vec_t &rhs,
              vec_t &res, real_t &res_norm)
{
    //real_t res_norm = 0;
    std::cout<<"residual"<<std::endl;
    vec_t lv;
    for (int i=0; i<v.size(); i++){
	mv(v, lv);
    res[i] = rhs[i] - lv[i];
    }
for (int i=0; i<res.size(); i++){
    res_norm += res[i];
}
res_norm = res_norm / res.size();
}


