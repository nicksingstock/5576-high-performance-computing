/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 11 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief
 */

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <unistd.h> // for sleep
#include "omp-utils.h"
#include "semaphore.h"

#define BSZ 5

int buffer[BSZ];
int next_put = 0;
int next_get = 0;

// create semaphores
Sem empty(BSZ, "empty");
Sem filled(0, "filled");


void put(int value) {
    empty.wait();
    int idx;
    idx = next_put;
    #pragma omp critical
    filled.post();
    next_put = (next_put + 1) % BSZ;
    buffer[idx] = value;
}

int get() {
    filled.wait();
    int idx;
    idx = next_get;
    #pragma omp critical
    empty.post();
    next_get = (next_get + 1) % BSZ;
    return buffer[idx];
}

void* producer(int nprod, int rank) {
    TID("Starting producer");

    for (int i = 0; i < nprod; i++){
        put(nprod*rank+i+1);
    }

    TID("Producer done");
}

void* consumer(int nprod, int rank) {
    TID("Starting consumer");

    for (int i = 0; i < nprod; i++) {
        int b = get();
        usleep(1e2); //consuming slower than producers
        printf("%05d\n",b);
    }

    TID("Consumer done");
}

int main(int argc, char *argv[])
{
    if(argc < 4 ){
        printf("Need three arguements num_products, num_producer, num_consumer.\n");
        return 1;
    }
    int num_product = atoi(argv[1]);
    int np = atoi(argv[2]);
    int nc = atoi(argv[3]);
    if (omp_get_max_threads()!=np+nc) abort();

    #pragma omp parallel
    {
        #pragma omp single
        {
            for (int pIdx(0); pIdx<np; ++pIdx){
                #pragma omp task
                producer(num_product, pIdx);
            }

            for (int cIdx(0); cIdx<nc; ++cIdx){
                #pragma omp task
                consumer(num_product, cIdx);
            }
        }
    }

    return 0;
}
